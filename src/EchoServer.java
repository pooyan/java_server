import java.io.*;
import java.nio.CharBuffer;

import javax.bluetooth.*;
import javax.microedition.io.*;
// bluecov lib from https://groups.google.com/forum/#!topic/bluecove-users/7jWv1V1GC-4

public class EchoServer {
    public final UUID uuid = new UUID(                              //the uid of the service, it has to be unique,
                                      "fa87c0d0afac11de8a390800200c9a66", false); //it can be generated randomly
    public final String name = "Echo Server";                       //the name of the service
    public final String url  =  "btspp://localhost:" + uuid         //the service url
    + ";name=" + name 
    + ";authenticate=false;encrypt=false;";
    LocalDevice local = null;
    StreamConnectionNotifier server = null;
    StreamConnection conn = null;
    mouse Mouse=new mouse();
    
    public EchoServer() {
    	
        try {
            System.out.println("Setting device to be discoverable...");
            local = LocalDevice.getLocalDevice();
            local.setDiscoverable(DiscoveryAgent.GIAC);
            System.out.println("Start advertising service...");
            server = (StreamConnectionNotifier)Connector.open(url);
            System.out.println("Waiting for incoming connection...");
            conn = server.acceptAndOpen();
            System.out.println("Client Connected...");
            DataInputStream din   = new DataInputStream(conn.openInputStream());
            
           Mouse.getdataforinit(din,100);
            
         //   Mouse.moveToMiddle();
        
            
            
            
            
            
            
                                                
        } catch (Exception  e) {System.out.println("Exception Occured: " + e.toString());}
    }
    
    public static void main (String args[]){
        EchoServer echoserver = new EchoServer(); 
    }
    
}