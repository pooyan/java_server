import java.io.*;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import javax.bluetooth.*;
import javax.microedition.io.*;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.PointerInfo;
import java.awt.Point;

public class mouse{

	public void getdataforinit(DataInputStream dstream,float NumberofSamples)
	{
		float[] myFloatArray = {0,0,0,0,0,0,0,0,0};
		float[] myFloatArrayAvg= {0,0,0,0,0,0,0,0,0};
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		float width =(float) screenSize.getWidth();
		float height =(float) screenSize.getHeight();
		PointerInfo a;
		Point b;
		float[] avgCurrentAngle={0,0,0,0,0,0,0,0,0};
		float i=0;
		String bigStringWithAllFloats;

		float accel3[]=new float[20];
		Arrays.fill(accel3,0);
		float accel4[]=new float[20];
		Arrays.fill(accel4,0);
		float accel5[]=new float[30];
		Arrays.fill(accel5,0);
		float gyro6[]=new float[20];
		Arrays.fill(gyro6,0);
		float gyro7[]=new float[20];
		Arrays.fill(gyro7,0);
		float gyro8[]=new float[20];
		Arrays.fill(gyro8,0);

		float[] accelDifference3;
		float[] accelDifference4;
		float[] accelDifference5;
		float[] gyroDifference6;
		float[] gyroDifference7;
		float[] gyroDifference8;

		float displacementArray[][]=new float[2][100];
		Arrays.fill(displacementArray[0], 1f);
		Arrays.fill(displacementArray[1], 1f);

		int pauseLength=21;

		try{
			BufferedReader buffer= new BufferedReader(new InputStreamReader(dstream));   
			//float[] angleDif={0,0,0};
			float[] angleDif_1;

			float[] angleDif_2 = {0, 0, 0};
			float[] angleDif_3 = {0, 0, 0};
			float[] angleDif_4 = {0, 0, 0};
			float[] angleDif_5 = {0, 0, 0};
			float[] angleDif_6 = {0, 0, 0};
			float[] angleDif_7 = {0, 0, 0};
			float[] angleDif_8 = {0, 0, 0};
			float[] angleDif_9 = {0, 0, 0};
			float[] angleDif_10 = {0, 0, 0};
			float[] angleDif_11 = {0, 0, 0};

			float w1 = 0.5f;
			float w2 = (float)Math.pow((double)w1,2);
			float w3 = (float)Math.pow((double)w2,2);
			float w4 = (float)Math.pow((double)w3,2);
			float w5 = (float)Math.pow((double)w4,2);
			float w6 = (float)Math.pow((double)w5,2);
			float w7 = (float)Math.pow((double)w6,2);
			float w8 = (float)Math.pow((double)w7,2);
			float w9 = (float)Math.pow((double)w8,2);
			float w10 = (float)Math.pow((double)w9,2);
			float w11 = (float)Math.pow((double)w10,2);

			float accelInit[]=new float[3];
			Arrays.fill(accelInit,0);

			float coef=1f;

			boolean pressState=false;

			while (true)
			{
				bigStringWithAllFloats=buffer.readLine();
				//System.out.println(i);
				if (i<NumberofSamples)  {
					myFloatArray=addTwoFloatArrays(stringtofloatarray(bigStringWithAllFloats,9),myFloatArray,9);
				}
				else if (i==NumberofSamples)  {
					for (int j=0;j<=2;j++){
						myFloatArrayAvg[j]=myFloatArray[j]/NumberofSamples;
						//System.out.println(avgCurrentAngle[j]-myFloatArrayAvg[j]);
					}
					moveToMiddle();
				}
				else    {
					a = MouseInfo.getPointerInfo();
					b=a.getLocation();
					avgCurrentAngle=stringtofloatarray(bigStringWithAllFloats,9);
					angleDif_1=subtractTwoFloatArrays(avgCurrentAngle,myFloatArrayAvg);

					if (angleDif_1[0]>180){
						angleDif_1[0]=angleDif_1[0]-360;
					}
					if (angleDif_1[0]<-180){
						angleDif_1[0]=angleDif_1[0]+360;
					}
					if (angleDif_1[1]>180){
						angleDif_1[1]=angleDif_1[1]-360;
					}
					if (angleDif_1[1]<-180){
						angleDif_1[1]=angleDif_1[1]+360;
					}

					//float angleDif[] ={w1 * angleDif_1[0] + w2 * angleDif_2[0] + w3 * angleDif_3[0] + w4 * angleDif_4[0], w1 * angleDif_1[1] + w2 * angleDif_2[1] + w3 * angleDif_3[1] + w4 * angleDif_4[1]};
					float angleDif[] ={w1 * angleDif_1[0] + w2 * angleDif_2[0] + w3 * angleDif_3[0]+ w4 * angleDif_4[0] + w5 * angleDif_5[0]+ w6 * angleDif_6[0] + w7 * angleDif_7[0]+ w8 * angleDif_8[0] + w9 * angleDif_9[0]+ w10 * angleDif_10[0] + w11 * angleDif_11[0], w1 * angleDif_1[1] + w2 * angleDif_2[1] + w3 * angleDif_3[1]+ w4 * angleDif_4[1] + w5 * angleDif_5[1]+ w6 * angleDif_6[1] + w7 * angleDif_7[1]+ w8 * angleDif_8[1] + w9 * angleDif_9[1]+ w10 * angleDif_10[1] + w11 * angleDif_11[1]};
					angleDif_11=angleDif_10;
					angleDif_10=angleDif_9;
					angleDif_9=angleDif_8;
					angleDif_8=angleDif_7;
					angleDif_7=angleDif_6;
					angleDif_6=angleDif_5;
					angleDif_5=angleDif_4;
					angleDif_4=angleDif_3;
					angleDif_3=angleDif_2;
					angleDif_2=angleDif_1;
					//float[] angleDif=subtractTwoFloatArrays(avgCurrentAngle,myFloatArrayAvg);

					if (angleDif[0]>180){
						angleDif[0]=angleDif[0]-360;
					}
					if (angleDif[0]<-180){
						angleDif[0]=angleDif[0]+360;
					}
					if (angleDif[1]>180){
						angleDif[1]=angleDif[1]-360;
					}
					if (angleDif[1]<-180){
						angleDif[1]=angleDif[1]+360;
					}

					float angleDifX=angleDif[0];

					float angleDifY=angleDif[1];

					float displacementX=(float)Math.pow((double)angleDifX,1)*width/(25*coef);

					float displacementY=(float)Math.pow((double)angleDifY,1.0)*height/(12*coef);


					//if (Math.abs(displacementX)<0.5||Math.abs(displacementY)<0.5)
					if (Math.abs(displacementX)<0.5||Math.abs(displacementY)<0.5||Math.abs(avgCurrentAngle[6])<0.007||Math.abs(avgCurrentAngle[7])<0.007||Math.abs(avgCurrentAngle[8])<0.007)
					{
						displacementX=0;  
						displacementY=0;
					}

					shiftArrayByOne(displacementArray[0], displacementX);

					shiftArrayByOne(displacementArray[1], displacementY); 

					float speed=getspeed(displacementArray[0][2], displacementArray[0][3], displacementArray[1][2], displacementArray[1][3]);

					coef=getSpeedCoefficent(speed);

					//	System.out.println(coef);

					if (coef<1)
					{
						coef=1;
					}
					int x = (int) b.getX();
					int y = (int) b.getY();
					//System.out.println(x+" "+y);
					if (x+displacementArray[0][2]>=0 & x+displacementArray[0][2]<=width & y+displacementArray[1][2]>=0 & y+displacementArray[1][2]<=height & pauseLength>22)
					{
						try{
							Robot robot = new Robot();
							robot.mouseMove((int)(x+displacementArray[0][2]),(int)(y+displacementArray[1][2])); 
						}
						catch (AWTException e)
						{
							e.printStackTrace();
						}
					}
					if (x+displacementArray[0][0]<0 & y+displacementArray[1][0]>=0 & y+displacementArray[1][0]<=height & pauseLength>22)
					{
						try{
							Robot robot = new Robot();
							robot.mouseMove((int)(1),(int)(y+displacementArray[1][0])); 
						}
						catch (AWTException e)
						{
							e.printStackTrace();
						}
					}
					if (x+displacementArray[0][0]>width & y+displacementArray[1][0]>=0 & y+displacementArray[1][0]<=height & pauseLength>22)
					{
						try{
							Robot robot = new Robot();
							robot.mouseMove((int)(width-1),(int)(y+displacementArray[1][0])); 
						}
						catch (AWTException e)
						{
							e.printStackTrace();
						}
					}
					if (x+displacementArray[0][0]>=0 & x+displacementArray[0][0]<=width & y+displacementArray[1][0]<0 & pauseLength>22)
					{
						try{
							Robot robot = new Robot();
							robot.mouseMove((int)(x+displacementArray[0][0]),(int)(1)); 
						}
						catch (AWTException e)
						{
							e.printStackTrace();
						}
					}
					if (x+displacementArray[0][0]>=0 & x+displacementArray[0][0]<=width & y+displacementArray[1][0]>height & pauseLength>22)
					{
						try{
							Robot robot = new Robot();
							robot.mouseMove((int)(x+displacementArray[0][0]),(int)(height-0.01)); 
						}
						catch (AWTException e)
						{
							e.printStackTrace();
						}
					}
					shiftArrayByOne(accel3,avgCurrentAngle[3]);

					accelDifference3=arrayDifference(accel3);

					shiftArrayByOne(accel4,avgCurrentAngle[4]);

					accelDifference4=arrayDifference(accel4);

					shiftArrayByOne(accel5,avgCurrentAngle[5]);

					accelDifference5=arrayDifference(accel5);

					shiftArrayByOne(gyro6,avgCurrentAngle[6]);

					gyroDifference6=arrayDifference(gyro6);

					shiftArrayByOne(gyro7,avgCurrentAngle[7]);

					gyroDifference7=arrayDifference(gyro7);

					shiftArrayByOne(gyro8,avgCurrentAngle[8]);

					gyroDifference8=arrayDifference(gyro8);

					/*if (((initCheckForEvent(makeAccelInitFromAccel(accel3,0,2))) ||
					 (initCheckForEvent(makeAccelInitFromAccel(accel4,0,2)) & initCheckForEvent(makeAccelInitFromAccel(accel4,1,2))) ||
					 (initCheckForEvent(makeAccelInitFromAccel(accel5,0,2))&))


					 )// & arrayVariations(makeAccelInitFromAccel(accelDifference,4,2))<1)
					 */
					if ((//(initCheckForEvent(makeAccelInitFromAccel(accel3,0,2))) ||
							(initCheckForEvent(makeAccelInitFromAccel(accel4,0,2)) & initCheckForEvent(makeAccelInitFromAccel(accel4,1,2))) ||
							(initCheckForEvent(makeAccelInitFromAccel(accel5,0,2))))&


							arrayVariations(makeAccelInitFromAccel(accelDifference5,2,4))<2f)// & arrayVariations(makeAccelInitFromAccel(accelDifference,4,2))<1)
					{
						//	System.out.println(makeAccelInitFromAccel(gyro6,0,2)[0]+" "+makeAccelInitFromAccel(gyro6,0,2)[1]);//+" "+makeAccelInitFromAccel(accelDifference,4,2)[0]+" "+makeAccelInitFromAccel(accelDifference,4,2)[1]);
						//System.out.println(displacementArray[0][1]+" "+displacementArray[0][0]);//+" "+makeAccelInitFromAccel(accelDifference,4,2)[0]+" "+makeAccelInitFromAccel(accelDifference,4,2)[1]);
						//displacementArray[0][2] = 0;


						displacementArray[0][1] = 0;

						displacementArray[0][0] = 0;
						//displacementArray[1][2] = 0;
						displacementArray[1][1] = 0;

						displacementArray[1][0] = 0;
						if (pauseLength>25)
						{
							pauseLength=0;
							//	System.out.println("now");
						}
					}

					pauseLength += 1;
					if (pauseLength==20)
					{
						//	for (int k=0;k<24;k++)
						//	{
						//	System.out.print(accel5[23-k]+" ");
						//	}
						System.out.println("now");
						System.out.println(actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[1][0]);

						System.out.println(actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[0][0]);

						
						if (pressState==false)
						{
							for (int k=0;k<accel5.length;k++)
							{
								//System.out.print(reverseArray(accel5)[k]-takeAverage(reverseArray(accel5), 0, 3)+" ");
								System.out.print(reverseArray(accel5)[k]+" ");

							}
							System.out.println();
							switch(actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][0])
							{
							case 0:
								if (actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1])
								{
									try{
										Robot robot = new Robot();
										rightClick(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}

									System.out.println("right click");
								}
							case 1: 
								if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))<2 & actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1])
								{
									//if ((actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[0][1]<actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[1][1]))
									//if (actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1])
									//{
									try{
										Robot robot = new Robot();
										rightClick(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}

									System.out.println("right click");


									//}
								}
								else if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))<2 & actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]<actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1]&actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]!=2)
								{
									//if ((actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[0][1]<actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[1][1]))
									//if (actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1])
									//{
									try{
										Robot robot = new Robot();
										leftClick(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}

									System.out.println("left click");


									//}
								}
								else if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))>2 & actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]<actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1]&actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]!=2)

								{
									try{
										Robot robot = new Robot();
										press(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}
									System.out.println("ready to drag");

									pressState=true;
								}
								else if(actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]==2&actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[1][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][1])
								{
									try{
										Robot robot = new Robot();
										doubleClick(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}
									System.out.println("double click");
								}
								else
								{
									System.out.println("nothing detected");

								}
								break;
							case 2: 
								//if ((actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[0][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[1][1]))
								if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))<2&actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]!=2)
								{
									try{
										Robot robot = new Robot();
										leftClick(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}

									System.out.println("left click");

								}

								else if (Math.abs(takeAverage(reverseArray(accel5),0, 3)-takeAverage(reverseArray(accel5), 25, 3))>2&actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]!=2)
								{
									try{
										Robot robot = new Robot();
										press(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}

									System.out.println("ready to drag");

									pressState=true;

								}
								else if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))<2&actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]==2)
								{
									try{
										Robot robot = new Robot();
										doubleClick(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}
									System.out.println("double click");
								}

								break;


							case 3: 

								if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))<2)
								{

									//if ((actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[0][1]>actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[1][1]))
									if (actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]==1)
									{

										try{
											Robot robot = new Robot();
											leftClick(robot);

										}
										catch (AWTException e)
										{
											e.printStackTrace();
										}
										System.out.println("left click");
									}
									else
									{


										try{
											Robot robot = new Robot();
											doubleClick(robot);

										}
										catch (AWTException e)
										{
											e.printStackTrace();
										}
										System.out.println("double click");
									}
								}
								else 
								{
									if (actionRecognition(makeAccelInitFromAccel(accel5, 0, 25))[0][0]==2)
									{
										try{
											Robot robot = new Robot();
											doubleClick(robot);

										}
										catch (AWTException e)
										{
											e.printStackTrace();
										}
										System.out.println("double click");	
									}
									else{
										try{
											Robot robot = new Robot();
											press(robot);

										}
										catch (AWTException e)
										{
											e.printStackTrace();
										}

										System.out.println("ready to drag");

										pressState=true;
									}
								}
								break;
							case 4:
								if (Math.abs(takeAverage(reverseArray(accel5), 25, 3)-takeAverage(reverseArray(accel5), 0, 3))<2)
								{

									if (actionRecognition(makeAccelInitFromAccel(accel5, 0, 23))[0][0]==2)
									{
										try{
											Robot robot = new Robot();
											doubleClick(robot);

										}
										catch (AWTException e)
										{
											e.printStackTrace();
										}
										System.out.println("double click");
									}
								}
								else
								{
									try{
										Robot robot = new Robot();
										press(robot);

									}
									catch (AWTException e)
									{
										e.printStackTrace();
									}
									System.out.println("ready to drag");

									pressState=true;
								}

								break;

							}
						}

						else
						{
							try{
								Robot robot = new Robot();
								release(robot);

							}
							catch (AWTException e)
							{
								e.printStackTrace();
							}
							System.out.println("released");

							pressState=false;

						}

					}
					myFloatArrayAvg=avgCurrentAngle; 

					float[] diff;


					// System.out.println(avgCurrentAngle[8]);

					//System.out.println(arrayVariations(makeAccelInitFromAccel(accelDifference5,2,2))+" "+arrayVariations(makeAccelInitFromAccel(accelDifference5,4,3)));
					// System.out.println(x+" "+y);   
				} 
				i=i+1;
			}
			/* for (i=0;i<2;i++){
			 myFloatArray[i]=myFloatArray[i]/NumberofSamples;
			 }*/
		}catch (Exception  e) {System.out.println("Exception Occured: " + e.toString());}
		//return myFloatArray;  
	}


	void moveToMiddle()
	{ 
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		try
		{
			Robot robot = new Robot();
			robot.mouseMove((int)width/2,(int)height/2); 
		}
		catch (AWTException e)
		{
			e.printStackTrace();
		}
	}
	float[] stringtofloatarray(String S,int length)
	{
		String regex=" ";
		float[] myFloatArray = new float[length];
		String[] tabOfFloatString = S.split(regex);
		int j=0;
		for(String s:tabOfFloatString){
			float res = Float.parseFloat(s);
			myFloatArray[j]=res+myFloatArray[j];
			j+=1;
		}
		return myFloatArray;
	}
	float[] addTwoFloatArrays(float[]a,float[]b,int size)
	{
		float[] c=new float[size];
		for (int i=0;i<size-1;i++)
		{
			c[i]=a[i]+b[i];
		}
		return c;
	}
	float[] subtractTwoFloatArrays(float[]a,float[]b)
	{
		float[] c=new float[3];
		for (int i=0;i<=2;i++)
		{
			c[i]=a[i]-b[i];
		}
		return c;
	}
	int[] screeninfo()
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		int[] size={(int)width,(int)height};
		return size;

	}


	void connect()
	{
		LocalDevice local = null;
		StreamConnectionNotifier server = null;
		StreamConnection conn = null;
		final UUID uuid = new UUID(                              //the uid of the service, it has to be unique,
				"fa87c0d0afac11de8a390800200c9a66", false); //it can be generated randomly
		final String name = "Echo Server";      
		final String url  =  "btspp://localhost:" + uuid         //the service url
				+ ";name=" + name 
				+ ";authenticate=false;encrypt=false;";    
		try{ 
			local = LocalDevice.getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC);
			System.out.println("Start advertising service...");
			server = (StreamConnectionNotifier)Connector.open(url);
			System.out.println("Waiting for incoming connection...");
			conn = server.acceptAndOpen();
			System.out.println("Client Connected...");
			DataInputStream din   = new DataInputStream(conn.openInputStream());
		}
		catch (Exception  e) {System.out.println("Exception Occured: " + e.toString());}
	}
	public boolean initCheckForEvent(float[] accel)
	{
		boolean check=false;
		if (getMaxValue(accel)-getMinValue(accel)>4.5f)
		{
			check=true;
		}
		return check;

	}
	public float[] makeAccelInitFromAccel(float[] accel,int start,int length)
	{
		float[] accelInit=new float[length];

		for (int i=start;i<length+start;i++)
		{
			accelInit[i-start]=accel[i];
		}

		return accelInit;

	}
	public float[] arrayDifference(float[] array)
	{
		float[] diffArray=new float[array.length-1];
		for (int i=0;i<array.length-1;i++)
		{
			diffArray[i]=Math.abs(array[i+1]-array[i]);
		}
		return diffArray;
	}

	public float arrayVariations(float[] array)
	{
		float sum=0;
		float var;
		float number=array.length;

		for (int i=0;i<array.length;i++)
		{
			sum=sum+array[i];
		}
		var=sum/(number);

		return var;
	}

	public int[][] actionRecognition(float[] signal)
	{
		int numOfMax=0;

		int numOfMin=0;

		float[] signal2=new float[signal.length];

		int[][] whichElements=new int[2][8];

		Arrays.fill(whichElements[0], 0);

		Arrays.fill(whichElements[1], 0);

		int maxIndex=1;

		int minIndex=1;

		for (int i=0;i<signal.length;i++)
		{
			signal2[i]=signal[signal.length-i-1];
		}


		for (int i=2;i<signal2.length-2;i++)
		{
			if (signal2[i]>signal2[i-1]&signal2[i]>signal2[i-2]&signal2[i]>=signal2[i+1]&signal2[i]>=signal2[i+2]&signal2[i]>takeAverage(signal2, 0, 3)+4)
			{
				numOfMax=numOfMax+1;

				whichElements[0][maxIndex]=i;

				maxIndex=maxIndex+1;
			}
			if (signal2[i]<signal2[i-1]&signal2[i]<signal2[i-2]&signal2[i]<=signal2[i+1]&signal2[i]<=signal2[i+2]&signal2[i]<takeAverage(signal2, 0, 3)-3)
			{
				numOfMin=numOfMin+1;

				whichElements[1][minIndex]=i;

				minIndex=minIndex+1;

			}
		}
		int[]numberOfInfs={numOfMax,numOfMin};

		whichElements[0][0]=numOfMax;
		whichElements[1][0]=numOfMin;

		return whichElements;

	}

	public static float getMinValue(float[] numbers){  
		float minValue = numbers[0];  
		for(int i=1;i<numbers.length;i++){  
			if(numbers[i] < minValue){  
				minValue = numbers[i];  
			}  
		}  
		return minValue;  
	}  
	public static float getMaxValue(float[] numbers){  
		float maxValue = numbers[0];  
		for(int i=1;i < numbers.length;i++){  
			if(numbers[i] > maxValue){  
				maxValue = numbers[i];  
			}  
		}  

		return maxValue;  
	}  
	void shiftArrayByOne(float[]array,float lastValue)
	{
		for (int i=1;i<=array.length-1;i++)
		{
			array[array.length-i]=array[array.length-i-1];
		}
		array[0]=lastValue;
	}

	void leftClick(Robot robot)
	{
		robot.mousePress(InputEvent.BUTTON1_MASK);

		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}

	void doubleClick(Robot robot)
	{
		robot.mousePress(InputEvent.BUTTON1_MASK);

		robot.mouseRelease(InputEvent.BUTTON1_MASK);

		robot.mousePress(InputEvent.BUTTON1_MASK);

		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}

	void rightClick(Robot robot)
	{
		robot.mousePress(InputEvent.BUTTON3_MASK);
		robot.mouseRelease(InputEvent.BUTTON3_MASK);
	}

	float[] reverseArray(float[] array)
	{
		float[] reversedArray=new float[array.length];
		for (int i=0;i<array.length;i++)
		{
			reversedArray[i]=array[array.length-i-1];
		}

		return reversedArray;

	}

	float takeAverage(float[]array,int start,int length)
	{
		float sum=0;
		for (int j=0;j<length;j++)
		{
			sum=sum+array[start+j];

		}
		sum=sum/length;
		return sum;

	}

	void press(Robot robot)
	{
		robot.mousePress(InputEvent.BUTTON1_MASK);
	}

	void release(Robot robot)
	{
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}

	float getSpeedCoefficent(float speed)
	{
		float coefficent;

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();


		float width =(float) screenSize.getWidth();

		coefficent=3-2*speed/(width/20f);

		return coefficent;
	}

	float getspeed(float x,float xprime,float y,float yprime)
	{
		double speed= Math.sqrt((double) (Math.pow((double)(x-xprime), 2)+Math.pow((double)(y-yprime), 2)));

		return (float) speed;
	}


	void init()
	{
	}
	void move()
	{
	}
}